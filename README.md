Notice
======
This script is mostly redundant, since Vega already exports PGN headers for each round into www[tournamentname]/round#.pgn. The only thing Vega misses currently is exporting national ratings (which this script will do). 

veg2pgn
=======
This script converts pairing in [Vega's](http://www.vegachess.com/) CSV format to PGN. The purpose is to easily bring players and pairing information from [Vega](http://www.vegachess.com/) into [DGT LiveChess](http://www.livechesscloud.com/), but it can also be used to quickly create the PGN headers if game moves are being entered manually (this saves the user from repeatedly typing in player names, ratings, etc).

Install
-------
Clone the repo and optionally add a symbolic link somewhere in your PATH. For example:
```sh
# clone repo (anywhere you like)
git clone https://gitlab.com/oshd/veg2pgn.git
# make link
sudo ln -s /path/to/cloned/veg2pgn /usr/local/bin/veg2pgn
```

Usage
-----
1) Export the round's pairing information from Vega: File -> Export -> Pairing (CSV)
2) Supply this CSV file to the veg2pgn script: `veg2pgn filename.csv`
3) Import the PGN file (named `pairings-round-#.pgn`) into LiveChess.

Player names and ratings do not need to be manually entered in LiveChess, as they will be imported automatically along with the current round.
